USE asterixdb;

-- question 1: liste des potions
SELECT * FROM potion;

SELECT "2. Liste des noms des trophées rapportant 3 points" AS QUESTION_2;
SELECT NomCateg, NbPoints FROM categorie WHERE NbPoints = 3;

SELECT "3. Liste des villages contenant plus de 35 huttes" AS QUESTION_3;
SELECT NomVillage from village WHERE NbHuttes > 35;

SELECT "4. Liste des trophées pris en mai-juin 1952" AS QUESTION_4;
SELECT * FROM trophee WHERE DatePrise BETWEEN '2052-05-01' AND '2052-06-12';

SELECT "5. Noms des habitants commençant par -a et contenant la lettre r" AS QUESTION_5;
SELECT * FROM habitant WHERE Nom LIKE 'a%r%';

SELECT "6. Numéros des habitants ayant bu les potions 1,3 ou 4" AS QUESTION_6;
SELECT DISTINCT NumHab from absorber WHERE NumPotion IN (1, 3, 4);

SELECT "7. Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur" AS QUESTION_7;
SELECT NumTrophee, DatePrise, NomCateg, Nom 
FROM trophee 
JOIN categorie ON trophee.CodeCat = categorie.CodeCat  
JOIN habitant ON trophee.NumPreneur = habitant.NumHab;

SELECT "8. Nom des habitants qui habitent à Aquilona" AS QUESTION_8;
SELECT Nom FROM habitant JOIN village ON habitant.NumVillage = village.NumVillage WHERE NomVillage = "Aquilona";

SELECT "9. Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat" AS QUESTION_9;
SELECT Nom FROM habitant
JOIN trophee ON trophee.NumPreneur = habitant.Numhab
JOIN categorie ON trophee.CodeCat = categorie.CodeCat WHERE NomCateg = "Bouclier de Légat";

SELECT "10. Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant principal" AS QUESTION_10;
SELECT LibPotion, Formule, ConstituantPrincipal FROM potion
JOIN fabriquer ON potion.NumPotion = fabriquer.NumPotion 
JOIN habitant ON habitant.NumHab = fabriquer.NumHab
WHERE Nom = "Panoramix";

SELECT "11. Liste des potions (libellés) absorbées par Homéopatix" AS QUESTION_11;
SELECT DISTINCT LibPotion FROM potion
JOIN absorber ON absorber.NumPotion = potion.NumPotion
JOIN habitant ON habitant.NumHab = absorber.NumHab
WHERE Nom = "Homéopatix";

SELECT "12. Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro 3" AS QUESTION_12;
SELECT DISTINCT habitant.Nom FROM habitant
JOIN absorber ON absorber.NumHab = habitant.NumHab
JOIN potion ON potion.NumPotion = absorber.NumPotion
JOIN fabriquer ON fabriquer.NumPotion = potion.NumPotion
WHERE fabriquer.NumHab = 3;

SELECT "13. Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix" AS QUESTION_13;
SELECT DISTINCT Nom FROM habitant
JOIN absorber ON absorber.NumHab = habitant.NumHab
JOIN fabriquer ON fabriquer.NumPotion = absorber.NumPotion
WHERE fabriquer.NumHab = 2;

SELECT "14. Nom des habitants dont la qualité n'est pas renseignée" AS QUESTION_14;
SELECT Nom FROM habitant
WHERE NumQualite IS NULL;

SELECT "15. Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la potion) en février 52" AS QUESTION_15;
SELECT Nom FROM habitant
JOIN absorber ON absorber.NumHab = habitant.NumHab
JOIN potion ON potion.NumPotion = absorber.NumPotion
WHERE potion.LibPotion = 'Potion magique n°1' AND MONTH(absorber.DateA) = 2 AND YEAR(absorber.DateA) = 2052;

SELECT "16. Nom et âge des habitants par ordre alphabétique" AS QUESTION_16;
SELECT Nom, Age FROM habitant
ORDER BY NOM ASC;

SELECT "17. Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom du village" AS QUESTION_17;
SELECT NomResserre, NomVillage FROM resserre
JOIN village ON village.NumVillage = resserre.NumVillage
ORDER BY Superficie DESC;

SELECT "18. Nombre d'habitants du village numéro 5" AS QUESTION_18;
SELECT COUNT(*) FROM habitant
WHERE habitant.NumVillage = 5;

SELECT "19. Nombre de points gagnés par Goudurix" AS QUESTION_19;
SELECT SUM(NbPoints) AS NbrePoints FROM categorie
JOIN trophee ON trophee.CodeCat = categorie.CodeCat
Join habitant ON trophee.NumPreneur = habitant.NumHab
WHERE habitant.Nom = "Goudurix";

SELECT "20. Date de première prise de trophée" AS QUESTION_20;
SELECT MIN(DatePrise) FROM trophee;

SELECT "21. Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées" AS QUESTION_21;
SELECT SUM(Quantite) AS Nombre_de_louches FROM absorber
JOIN potion ON potion.NumPotion = absorber.NumPotion
WHERE potion.LibPotion = 'Potion magique n°2';

SELECT "22. Superficie la plus grande" AS QUESTION_22;
SELECT MAX(Superficie) FROM resserre;

SELECT "23. Nombre d'habitants par village (nom du village, nombre)" AS QUESTION_23;
SELECT NomVillage, COUNT(NumHab) FROM habitant
JOIN village ON habitant.NumVillage = village.NumVillage
GROUP BY NomVillage;

SELECT "24. Nombre de trophées par habitant" as QUESTION_24;
SELECT Nom, COUNT(NumTrophee) AS Nombre_de_trophées FROM habitant
JOIN trophee ON trophee.NumPreneur = habitant.NumHab
GROUP BY NumHab;

SELECT "25. Moyenne d'âge des habitants par province (nom de province, calcul)" AS QUESTION_25;
SELECT NomProvince, AVG(Age) FROM habitant
JOIN village ON village.NumVillage = habitant.NumVillage
JOIN province ON province.NumProvince = village.NumProvince
GROUP BY NomProvince;

SELECT "26. Nombre de potions différentes absorbées par chaque habitant (nom et nombre)" AS QUESTION_26;
SELECT Nom, COUNT(DISTINCT absorber.NumPotion) AS Nombre_de_potions FROM habitant
JOIN absorber ON absorber.NumHab = habitant.NumHab
GROUP BY Nom;

SELECT "27. Nom des habitants ayant bu plus de 2 louches de potion zen" AS QUESTION_27;
SELECT Nom FROM habitant
JOIN absorber ON absorber.NumHab = habitant.NumHab
JOIN potion ON potion.NumPotion = absorber.NumPotion
WHERE LibPotion = 'Potion Zen' AND Quantite > 2;

SELECT "28. Noms des villages dans lesquels on trouve une resserre" AS QUESTION_28;
SELECT NomVillage FROM village
JOIN resserre ON resserre.NumVillage = village.NumVillage;

SELECT "29. Nom du village contenant le plus grand nombre de huttes" AS QUESTION_29;
SELECT NomVillage From village
WHERE NbHuttes = (SELECT(MAX(NbHuttes)) FROM village);

SELECT "30. Noms des habitants ayant pris plus de trophées qu'Obélix" AS QUESTION_30;
-- Nombre de trophées pris par Obélix
SELECT Count(NumTrophee) AS Nombre_de_trophees FROM trophee 
JOIN habitant ON trophee.NumPreneur = habitant.NumHab
WHERE habitant.Nom = "Obélix";
SELECT Nom, Count(NumTrophee) AS Nombre_de_trophees FROM habitant
JOIN trophee ON trophee.NumPreneur = habitant.NumHab
GROUP BY Nom
HAVING  Count(NumTrophee) > (SELECT Count(NumTrophee) AS Nombre_de_trophees FROM trophee 
JOIN habitant ON trophee.NumPreneur = habitant.NumHab
WHERE habitant.Nom = "Obélix");

